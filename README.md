# Linux DDNS Dynu
## Description

This is an Ansible Role that you can add to any playbook in order to install a standard setup of the Dynu DDNS client on Debian-based and RPM-based Linux distros.

Tested on CentOS 7 and Ubuntu 20.04.

## Dependencies

None

## Usage

In order to add this role on a playbook simply add it on the `roles` section or as an `include_role` directive inside one of the tasks files

#### Playbook example

```yaml
--- # Simple dynu playbook
- hosts: all
  become: yes
  become_method: sudo
  connection: ssh
  gather_facts: yes
  roles:
    - lx-ddns-dynu
```

#### Tasks File example

```yaml
---
- name: Include Dynu Role
  include_role:
    - name: lx-dynu # or the name of the directory where you cloned this repository
```


